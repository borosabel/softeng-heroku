FROM node:14.15.4

WORKDIR /usr/src/softeng

COPY package.json .

RUN npm install

COPY . .

EXPOSE 3000

CMD ["node", "app.js"]
